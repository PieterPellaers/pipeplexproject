Feature: Plex Web And Mobile Application

  @PlexMobile @working @severity=normal
    #check on username logged in
  Scenario: as user I want to be able to add a movie to my watchlist on mobile and see that movie in my watchlist on desktop and remove the movie from my watchlist afterwards
    Given   I am signed in on Plex mobile
    And     I search for a movie on Plex mobile
    And     I add a movie to my watchlist on mobile
    When    I am logged in as a user on desktop
    And     I launch the Plex desktop app
    And     the movie is in my watchlist on desktop
    And     I remove the movie from my watchlist
    Then    my watchlist should be empty