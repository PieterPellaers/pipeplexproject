Feature: Plex Web Application

  @PlexWeb @working @severity=blocker
    #check on username logged in
  Scenario: as user I want to be able to log in
    Given   I am on the Plex homepage
    And     I go to the sign in page
    When    I log in as a user
    Then    the correct user should be logged in

  @PlexWeb @working @severity=critical
    #check on movie title
  Scenario: as user I want to be able to search for a movie
    Given   I am logged in as a user on desktop
    When    I launch the Plex desktop app
    And     I search for a movie in the searchbar
    Then    the correct movie page is opened

  @PlexWeb @working @severity=critical
  Scenario: as user I want to be able to play a movie
    #check if pause button is visible
    Given   I am logged in as a user on desktop
    When    I launch the Plex desktop app
    And     I search for a movie in the searchbar
    And     the correct movie page is opened
    And     I hit the Play button
    Then    the movie starts playing

  @PlexWeb @working @severity=normal
    #check if number of movies is 1 when added and empty when removed
  Scenario: as user I want to be able to add and remove a movie from my watchlist
    Given   I am logged in as a user on desktop
    When    I launch the Plex desktop app
    And     I search for a movie in the searchbar
    And     the correct movie page is opened
    And     I hit the add to watchlist button
    And     the movie is in my watchlist on desktop
    And     I remove the movie from my watchlist
    Then    my watchlist should be empty

