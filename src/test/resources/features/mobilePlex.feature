Feature: Plex Mobile Application

  @PlexMobile @working @severity=blocker
    #check on username logged in
  Scenario: as user I want to be able to log in on Plex mobile
    Given   I open the Plex mobile app
    And     I go to the mobile sign in page
    When    I log in on mobile as a user
    Then    the correct user should be logged in on mobile

  @PlexMobile @working @severity=critical
  Scenario: as user I want to be able to search for a movie on Plex mobile
    #check on movie title
    Given   I am signed in on Plex mobile
    When    I search for a movie on Plex mobile
    Then    the correct movie page is opened on mobile

  @PlexMobile @working @severity=critical
  Scenario: as user I want to be able to play a movie
    #check if pause button is visible
    Given   I am signed in on Plex mobile
    When    I search for a movie on Plex mobile
    And     I tap the Play button
    Then    the movie starts playing on mobile