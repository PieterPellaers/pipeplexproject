package be.ordina.steps;

import be.ordina.plexMobile.pages.MobileHomePage;
import be.ordina.plexMobile.pages.MobileMenuPage;
import be.ordina.plexMobile.pages.MobileSignInPage;
import be.ordina.plexWeb.pages.WebAppPage;
import be.ordina.plexWeb.pages.WebHomePage;
import be.ordina.plexWeb.pages.WebSignInPage;
import be.ordina.utils.DriverSetup;
import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

public class PlexMobileSteps extends DriverSetup{

    MobileHomePage mobileHomepage;
    MobileSignInPage mobileSignInPage;
    MobileMenuPage mobileMenuPage;

    @Given("I open the Plex mobile app")
    public void openPlexMobile() throws MalformedURLException {
        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("deviceName", "Android Emulator");
        cap.setCapability("platformVersion", "11");
        cap.setCapability("platformName", "Android");

        cap.setCapability("appPackage", "com.plexapp.android");
        cap.setCapability("appActivity", "com.plexapp.plex.activities.SplashActivity");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");

        DriverSetup.driver = new AndroidDriver(url, cap);

        mobileHomepage = new MobileHomePage(DriverSetup.driver);
        mobileSignInPage = new MobileSignInPage(DriverSetup.driver);
        mobileMenuPage = new MobileMenuPage(DriverSetup.driver);
    }

    @And("I go to the mobile sign in page")
    public void openMobileSignIn() {
        mobileHomepage.openMenu();
        mobileMenuPage.openSignIn();
    }

    @When("I log in on mobile as a user")
    public void loginMobile() {
        mobileSignInPage.login("pieter.pellaers@gmail.com", "Banaan123?");
    }

    @Then("the correct user should be logged in on mobile")
    public void checkUsername() {
        mobileHomepage.openMenu();
        String username = mobileMenuPage.getUsername();
        assertTrue(username.equals("pieter.pellaers@gmail.com"));
    }

    @Given("I am signed in on Plex mobile")
    public void signInMobile() throws MalformedURLException {
        openPlexMobile();
        openMobileSignIn();
        loginMobile();
    }

    @When("I search for a movie on Plex mobile")
    public void searchMovie(){
        mobileHomepage.searchMovie("4 mile");
    }

    @Then("the correct movie page is opened on mobile")
    public void checkMovieTitle(){
        String movieTitle = mobileHomepage.getMovieTitle();
        assertTrue(movieTitle.equals("4 Minute Mile"));
    }

    @And("I tap the Play button")
    public void playFullMovie(){
        mobileHomepage.playFullMovie();
    }

    @Then("the movie starts playing on mobile")
    public void checkPauseButton(){
        mobileHomepage.togglePlayerOptions();
        mobileHomepage.pauseMovieBtnVisible();
    }

    @And("I add a movie to my watchlist on mobile")
    public void addMovieToWatchlist(){
        mobileHomepage.watchlistToggle();
    }
}
