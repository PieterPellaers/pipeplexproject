package be.ordina.steps;

import be.ordina.plexWeb.pages.WebAppPage;
import be.ordina.plexWeb.pages.WebHomePage;
import be.ordina.plexWeb.pages.WebSignInPage;
import be.ordina.utils.DriverSetup;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertTrue;

public class PlexWebSteps extends DriverSetup{

    WebHomePage webHomePage;
    WebSignInPage webSignInPage;
    WebAppPage webAppPage;

    @Given("I am on the Plex homepage")
    public void openHomePage(){
        System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Chromedriver\\chromedriver.exe");
        DriverSetup.driver = new ChromeDriver();

        //System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Geckodriver\\geckodriver.exe");
        //DriverSetup.driver = new FirefoxDriver();

        //System.setProperty("webdriver.edge.driver", "C:\\Program Files\\Edgedriver\\msedgedriver.exe");
        //DriverSetup.driver = new EdgeDriver();
        DriverSetup.driver.get("https://www.plex.tv/");

        webHomePage = new WebHomePage(DriverSetup.driver);
        webSignInPage = new WebSignInPage(DriverSetup.driver);
        webAppPage = new WebAppPage(DriverSetup.driver);
    }

    @And("I go to the sign in page")
    public void openSigInPage(){
        webHomePage.clickSignIn();
    }

    @When("I log in as a user")
    public void login(){
        webSignInPage.login("pieter.pellaers@gmail.com", "Banaan123?");
    }

    @Then("the correct user should be logged in")
    public void checkUser(){
        String user = webHomePage.getUser();

        assertTrue(user.equals("pieter.pellaers@gmail.com"));
        System.out.println("current user = " + user);
    }

    @Given("I am logged in as a user on desktop")
    public void signIn(){
        openHomePage();
        openSigInPage();
        login();
    }

    @When("I launch the Plex desktop app")
    public void launchApp(){
        webHomePage.clickLaunch();
    }

    @And("I search for a movie in the searchbar")
    public void searchMovie(){
        webAppPage.searchMovie("4 mile");
    }

    @Then("the correct movie page is opened")
    public void checkMovieTitle() {
        String title = webAppPage.getMovieTitle();

        assertTrue(title.equals("4 Minute Mile"));
        System.out.println("current selected movie = " + title);
    }

    @And("I hit the Play button")
    public void playMovie(){
        webAppPage.playFullMovie();
    }

    @Then("the movie starts playing")
    public void checkMovieIsPlaying(){
        webAppPage.checkMovieIsPlaying();
    }

    @And("I hit the add to watchlist button")
    public void addToWatchlist(){
        webAppPage.addToWatchlist();

    }

    @Then("the movie is in my watchlist on desktop")
    public void checkWatchlistCount() {
        webAppPage.openWatchlist();
        System.out.println("watchlistcount = " + webAppPage.getWatchlistCounter());
        String watchlistCount = webAppPage.getWatchlistCounter();
        assertTrue(watchlistCount.equals("1"));

    }

    @And("I remove the movie from my watchlist")
    public void removeFromWatchlist(){
        webAppPage.searchMovie("4 mile");
        webAppPage.removeFromWatchlist();
    }

    @Then("my watchlist should be empty")
    public void checkEmptyWatchlist(){
        webAppPage.openWatchlist();
        System.out.println("watchlistcount = " + webAppPage.getWatchlistCounter());
        String watchlistCount = webAppPage.getWatchlistCounter();
        assertTrue(watchlistCount.equals(""));
    }
}

