package be.ordina.steps;

import be.ordina.utils.DriverSetup;
import io.cucumber.core.api.Scenario;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;;

public class CommonSteps extends DriverSetup {

    @Before
    public void setUp(Scenario scenario){
        System.out.println("================================================");
        System.out.println("Scenario started: " + scenario.getName());
        System.out.println("================================================");
    }

    @After
    public void tearDown(Scenario scenario){
        DriverSetup.driver.quit();
        System.out.println("================================================");
        System.out.println("Scenario ended: " + scenario.getName());
        System.out.println("================================================");
    }
}
