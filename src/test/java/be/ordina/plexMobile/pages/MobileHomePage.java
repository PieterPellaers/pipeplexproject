package be.ordina.plexMobile.pages;


import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;


public class MobileHomePage {

    private final WebDriver mobileDriver;
    private WebDriverWait wait;

    private By btnOpenMenu = By.xpath("//android.widget.ImageButton[@content-desc=\"Navigate up\"]");

    private By btnSearch = By.id("com.plexapp.android:id/search");
    private By txtSearch = By.id("com.plexapp.android:id/search_src_text");
    private By btnFirstMovieResult = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/androidx.recyclerview.widget.RecyclerView/android.widget.RelativeLayout[1]");

    private By btnWatchlist = By.xpath("//android.widget.FrameLayout[@content-desc=\"Watchlist\"]");

    private By txtMovieTitle = By.id("com.plexapp.android:id/title");

    private By btnPlayFullMovie = By.id("com.plexapp.android:id/promoted_action");
    private By btnAddToWatchlist = By.id("com.plexapp.android:id/add_to_watchlist");
    private By btnMarkAsWatched = By.id("com.plexapp.android:id/mark_as");

    private By mainMovieScreen = By.id("com.plexapp.android:id/layout");
    private By btnPauseMovie = By.id("com.plexapp.android:id/play");
    private By btnPlayMovie = By.id("com.plexapp.android:id/play");
    private By btnCloseMovie = By.id("com.plexapp.android:id/action_close");



    public MobileHomePage(WebDriver mobileDriver) {
        this.mobileDriver = mobileDriver;
        this.wait = new WebDriverWait(mobileDriver, 30);
    }

    public void openMenu() {
        wait.until(ExpectedConditions.elementToBeClickable(btnOpenMenu));
        mobileDriver.findElement(btnOpenMenu).click();
    }

    public void searchMovie(String movieTitle){
        wait.until(ExpectedConditions.elementToBeClickable(btnSearch));
        mobileDriver.findElement(btnSearch).click();
        wait.until(ExpectedConditions.elementToBeClickable(txtSearch));
        mobileDriver.findElement(txtSearch).sendKeys(movieTitle);
        wait.until(ExpectedConditions.elementToBeClickable(btnFirstMovieResult));
        mobileDriver.findElement(btnFirstMovieResult).click();
    }

    public String getMovieTitle(){
        wait.until(ExpectedConditions.elementToBeClickable(txtMovieTitle));
        String movieTitle = mobileDriver.findElement(txtMovieTitle).getText();
        return movieTitle;
    }

    public void watchlistToggle() {
        wait.until(ExpectedConditions.elementToBeClickable(btnAddToWatchlist));
        mobileDriver.findElement(btnAddToWatchlist).click();
    }

    public void togglePlayerOptions() {
        wait.until(ExpectedConditions.elementToBeClickable(mainMovieScreen));
        mobileDriver.findElement(mainMovieScreen).click();
    }
      
    public void playFullMovie() {
        wait.until(ExpectedConditions.elementToBeClickable(btnPlayFullMovie));
        mobileDriver.findElement(btnPlayFullMovie).click();
    }

    public void pauseMovie() {
        wait.until(ExpectedConditions.elementToBeClickable(btnPauseMovie));
        mobileDriver.findElement(btnPauseMovie).click();
    }

    public void playMovie() {
        wait.until(ExpectedConditions.elementToBeClickable(btnPlayMovie));
        mobileDriver.findElement(btnPlayMovie).click();
    }

    public void closeMovie() {
        wait.until(ExpectedConditions.elementToBeClickable(btnCloseMovie));
        mobileDriver.findElement(btnCloseMovie).click();
    }

    public void openWatchlist(){
        wait.until(ExpectedConditions.elementToBeClickable(btnOpenMenu));
        mobileDriver.findElement(btnOpenMenu).click();
    }
    public void pauseMovieBtnVisible() {
        wait.until(ExpectedConditions.elementToBeClickable(btnPauseMovie));
        assertTrue(mobileDriver.findElement(btnPauseMovie).isDisplayed());
    }

}


