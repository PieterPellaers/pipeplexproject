package be.ordina.plexMobile.pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MobileSignInPage {

    private final WebDriver mobileDriver;
    private WebDriverWait wait;

    private By txtUsername = By.id("com.plexapp.android:id/username");
    private By txtPassword = By.id("com.plexapp.android:id/password");
    private By btnLogIn = By.id("com.plexapp.android:id/sign_in");

    public MobileSignInPage(WebDriver mobileDriver) {
        this.mobileDriver = mobileDriver;
        this.wait = new WebDriverWait(mobileDriver, 30);
    }

    public void login(String username, String password){
        wait.until(ExpectedConditions.elementToBeClickable(txtUsername));
        mobileDriver.findElement(txtUsername).sendKeys(username);
        mobileDriver.findElement(txtPassword).sendKeys(password);
        mobileDriver.findElement(btnLogIn).click();
    }
}
