package be.ordina.plexMobile.pages;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MobileMenuPage {

    private final WebDriver mobileDriver;
    private WebDriverWait wait;

    private By btnSignIn = By.id("com.plexapp.android:id/username");
    private By btnContinuePlexAccount = By.id("com.plexapp.android:id/continue_with_email");

    private By btnMoviesAndShows = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]");
    private By btnHome = By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.drawerlayout.widget.DrawerLayout/android.widget.FrameLayout/android.widget.RelativeLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]");

    public MobileMenuPage(WebDriver mobileDriver) {
        this.mobileDriver = mobileDriver;
        this.wait = new WebDriverWait(mobileDriver, 30);
    }

    public void openSignIn() {
        wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));
        mobileDriver.findElement(btnSignIn).click();
        wait.until(ExpectedConditions.elementToBeClickable(btnContinuePlexAccount));
        mobileDriver.findElement(btnContinuePlexAccount).click();
    }

    public String getUsername(){
        wait.until(ExpectedConditions.elementToBeClickable(btnSignIn));
        String username = mobileDriver.findElement(btnSignIn).getText();
        return username;
    }

    public void openHome() {
        wait.until(ExpectedConditions.elementToBeClickable(btnHome));
        mobileDriver.findElement(btnHome).click();
    }

    public void openMoviesAndShows() {
        wait.until(ExpectedConditions.elementToBeClickable(btnMoviesAndShows));
        mobileDriver.findElement(btnMoviesAndShows).click();
    }
}
