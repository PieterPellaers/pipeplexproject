package be.ordina.plexMobile;

import be.ordina.plexMobile.pages.MobileHomePage;
import be.ordina.plexMobile.pages.MobileMenuPage;
import be.ordina.plexMobile.pages.MobileSignInPage;
import be.ordina.plexWeb.pages.WebHomePage;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertTrue;

public class PlexMobileTest {

    private AndroidDriver mobileDriver;
    private WebDriverWait wait;

    @Before
    public void mobileSetup() throws MalformedURLException {

        DesiredCapabilities cap = new DesiredCapabilities();

        cap.setCapability("deviceName", "Android Emulator");
        cap.setCapability("platformVersion", "11");
        cap.setCapability("platformName", "Android");

        cap.setCapability("appPackage", "com.plexapp.android");
        cap.setCapability("appActivity", "com.plexapp.plex.activities.SplashActivity");

        URL url = new URL("http://127.0.0.1:4723/wd/hub");

        mobileDriver = new AndroidDriver(url, cap);

    }

    @Test
    public void mobileMainTest() {
        MobileHomePage mobileHomepage = new MobileHomePage(mobileDriver);
        MobileSignInPage mobileSignInPage = new MobileSignInPage(mobileDriver);
        MobileMenuPage mobileMenuPage = new MobileMenuPage(mobileDriver);

        //open hamburger menu
        mobileHomepage.openMenu();

        //log in
        mobileMenuPage.openSignIn();
        mobileSignInPage.login("pieter.pellaers@gmail.com", "Banaan123?");

        mobileHomepage.openMenu();
        String username = mobileMenuPage.getUsername();
        assertTrue(username.equals("pieter.pellaers@gmail.com"));

        //go to home page
        mobileMenuPage.openMoviesAndShows();

        //search movie
        mobileHomepage.searchMovie("4 mile");
        String movieTitle = mobileHomepage.getMovieTitle();
        assertTrue(movieTitle.equals("4 Minute Mile"));

        //add to watchlist
        mobileHomepage.watchlistToggle();

        //play movie
        mobileHomepage.playFullMovie();

        //pause movie
        mobileHomepage.togglePlayerOptions();
        mobileHomepage.pauseMovie();

        //close movie
        mobileHomepage.closeMovie();

        //remove from watchlist
        mobileHomepage.watchlistToggle();
        mobileHomepage.openMenu();

        //check watchlist
        mobileHomepage.openMenu();
        mobileMenuPage.openMoviesAndShows();
        mobileHomepage.openWatchlist();

    }

}
