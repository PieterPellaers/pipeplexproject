package be.ordina.plexWeb.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
public class WebHomePage {

    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnSignIn = By.className("signin");
    private By btnLaunch = By.xpath("//a[@class='launch button']");
    private By btnMyAccount = By.id("hideshow");
    private By txtUser = By.cssSelector("div.large-11:nth-child(1) > div:nth-child(1)");

    public WebHomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void clickSignIn(){
        driver.findElement(btnSignIn).click();
    }

    public void clickLaunch(){
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.elementToBeClickable(btnLaunch));
        driver.findElement(btnLaunch).click();
        }

    public String getUser(){
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.elementToBeClickable(btnMyAccount));
        driver.findElement((btnMyAccount)).click();
        String user = driver.findElement(txtUser).getText();
        return user;
    }
}
