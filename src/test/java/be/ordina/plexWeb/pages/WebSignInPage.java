package be.ordina.plexWeb.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebSignInPage {

    private final WebDriver driver;
    private WebDriverWait wait;

    private By txtUsername = By.cssSelector("#email");
    private By txtPassword = By.cssSelector("#password");
    private By btnLogIn = By.cssSelector("form._2kPyg > button");

    public WebSignInPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void login(String username, String password){
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        wait.until(ExpectedConditions.elementToBeClickable(txtUsername));
        driver.findElement(txtUsername).sendKeys(username);
        driver.findElement(txtPassword).sendKeys(password);
        driver.findElement(btnLogIn).click();
    }
}
