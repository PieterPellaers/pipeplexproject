package be.ordina.plexWeb.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.junit.Assert.assertTrue;

public class WebAppPage {

    private final WebDriver driver;
    private WebDriverWait wait;

    private By fldSearchBar = By.className("QuickSearchInput-searchInput-3m6naA");
    private By btnFirstMovieResult = By.xpath("/html/body/div/div[2]/div/div[1]/div/div[2]/div[2]/div/div");

    private By btnMenuMoviesShows = By.linkText("Movies & Shows");
    private By btnWatchlist = By.xpath("/html/body/div/div[4]/div/div/div[2]/div[1]/div[2]/div[1]/a[2]");
    private By txtWatchlistCounter = By.xpath("/html/body/div/div[4]/div/div/div[2]/div[2]/div[1]/span[2]");

    private By txtMovieTitle = By.className("PrePlayLeftTitle-leftTitle-3RWvGy");
    private By btnPlayFullMovie = By.id("plex-icon-toolbar-play-560");
    private By btnAddWatchlist = By.id("plex-icon-toolbar-add-to-watchlist-560");
    private By btnRemoveWatchlist = By.id("plex-icon-toolbar-remove-from-watchlist-560");
    private By btnMarkedWatched = By.id("plex-icon-toolbar-mark-played-560");

    private By btnFullscreen = By.id("plex-icon-player-fullscreen-560");
    private By btnWindowed = By.id("plex-icon-player-windowed-560");

    private By btnPlay = By.id("plex-icon-player-play-560");
    private By btnPause = By.id("plex-icon-player-pause-560");
    private By btnCloseMovie = By.id("plex-icon-player-stop-560");


    public WebAppPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,10);
    }

    public void searchMovie(String movieTitle){
        wait.until(ExpectedConditions.elementToBeClickable(fldSearchBar));
        driver.findElement(fldSearchBar).sendKeys(movieTitle);
        wait.until(ExpectedConditions.elementToBeClickable(btnFirstMovieResult));
        driver.findElement(btnFirstMovieResult).click();
    }

    public void openWatchlist(){
        wait.until(ExpectedConditions.elementToBeClickable(btnMenuMoviesShows));
        driver.findElement(btnMenuMoviesShows).click();
        wait.until(ExpectedConditions.elementToBeClickable(btnWatchlist));
        driver.findElement(btnWatchlist).click();
    }

    public String getWatchlistCounter(){
        wait.until(ExpectedConditions.presenceOfElementLocated(txtWatchlistCounter));
        String watchlistCounter = driver.findElement(txtWatchlistCounter).getText();
        return watchlistCounter;
    }

    public String getMovieTitle(){
        wait.until(ExpectedConditions.elementToBeClickable(txtMovieTitle));
        String title = driver.findElement(txtMovieTitle).getText();
        return title;
    }

    public void playFullMovie(){
        wait.until(ExpectedConditions.elementToBeClickable(btnPlayFullMovie));
        driver.findElement(btnPlayFullMovie).click();
    }

    public void addToWatchlist(){
        wait.until(ExpectedConditions.elementToBeClickable(btnAddWatchlist));
        driver.findElement(btnAddWatchlist).click();
    }

    public void removeFromWatchlist(){
        wait.until(ExpectedConditions.elementToBeClickable(btnRemoveWatchlist));
        driver.findElement(btnRemoveWatchlist).click();
    }

    public void markAsWatched(){
        wait.until(ExpectedConditions.elementToBeClickable(btnMarkedWatched));
        driver.findElement(btnMarkedWatched).click();
    }

    public void fullscreen(){
        wait.until(ExpectedConditions.presenceOfElementLocated(btnFullscreen));
        driver.findElement(btnFullscreen).click();
    }

    public void windowed(){
        wait.until(ExpectedConditions.elementToBeClickable(btnWindowed));
        driver.findElement(btnWindowed).click();
    }

    public void playMovie(){
        wait.until(ExpectedConditions.elementToBeClickable(btnPlay));
        driver.findElement(btnPlay).click();
    }

    public void pauseMovie(){
        wait.until(ExpectedConditions.elementToBeClickable(btnPause));
        driver.findElement(btnPause).click();
    }

    public void closeMovie(){
        wait.until(ExpectedConditions.elementToBeClickable(btnCloseMovie));
        driver.findElement(btnCloseMovie).click();
    }

    public void checkMovieIsPlaying(){
        wait.until(ExpectedConditions.elementToBeClickable(btnPause));
        assertTrue(driver.findElement(btnPause).isDisplayed());
    }
}
