package be.ordina.plexWeb;
import be.ordina.plexWeb.pages.WebAppPage;
import be.ordina.plexWeb.pages.WebHomePage;
import be.ordina.plexWeb.pages.WebSignInPage;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;

import static org.junit.Assert.assertTrue;

public class PlexWebTest {

    private WebDriver driver;

    @Before
    public void webSetup() {
        //System.setProperty("webdriver.chrome.driver", "C:\\Program Files\\Chromedriver\\chromedriver.exe");
        //driver = new ChromeDriver();

        //System.setProperty("webdriver.gecko.driver", "C:\\Program Files\\Geckodriver\\geckodriver.exe");
        //driver = new FirefoxDriver();

        System.setProperty("webdriver.edge.driver", "C:\\Program Files\\Edgedriver\\msedgedriver.exe");
        driver = new EdgeDriver();

        driver.get("https://www.plex.tv/");

    }


    @Test
    public void maintest() throws InterruptedException {
        WebHomePage webHomePage = new WebHomePage(driver);
        WebSignInPage webSignInPage = new WebSignInPage(driver);

        //login
        webHomePage.clickSignIn();
        webSignInPage.login("pieter.pellaers@gmail.com", "Banaan123?");

        String user = webHomePage.getUser();

        assertTrue(user.equals("pieter.pellaers@gmail.com"));
        System.out.println("current user = " + user);

        //LaunchApp
        webHomePage.clickLaunch();
        assertTrue(driver.getCurrentUrl().equals("https://app.plex.tv/desktop"));

        //Search a movie
        WebAppPage webAppPage = new WebAppPage(driver);
        webAppPage.searchMovie("4 mile");

        String title = webAppPage.getMovieTitle();

        assertTrue(title.equals("4 Minute Mile"));
        System.out.println("current selected movie = " + title);

        //add to watchlist
        webAppPage.addToWatchlist();
        Thread.sleep(2000);

        //check watchlist
        webAppPage.openWatchlist();
        System.out.println("watchlistcount = " + webAppPage.getWatchlistCounter());
        String watchlistCount = webAppPage.getWatchlistCounter();
        assertTrue(watchlistCount.equals("1"));

        //play movie
        webAppPage.searchMovie("4 mile");
        webAppPage.playFullMovie();

        //setFullscreen
        webAppPage.fullscreen();

        //pause movie
        webAppPage.pauseMovie();

        //exit fullscreen
        webAppPage.windowed();

        //close movie
        webAppPage.closeMovie();

        //Remove from watchlist
        webAppPage.removeFromWatchlist();
        Thread.sleep(2000);

        //check watchlist
        webAppPage.openWatchlist();
        System.out.println("watchlistcount = " + webAppPage.getWatchlistCounter());
        assertTrue(webAppPage.getWatchlistCounter().equals(""));
    }
}
